package com.riadh.docker;

import com.riadh.docker.entities.Reservation;
import com.riadh.docker.entities.ReservationStatus;
import com.riadh.docker.repository.ReservationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(ReservationRepository reservationRepository) {
        return args -> {
            reservationRepository.save(Reservation.builder().titre("Res 1").price(123).status(ReservationStatus.CREATED).build());
            reservationRepository.save(Reservation.builder().titre("Res 2").price(123555).status(ReservationStatus.CONFORMED).build());
            reservationRepository.save(Reservation.builder().titre("Res 3").price(10).status(ReservationStatus.CANCELED).build());
        };
    }
}
